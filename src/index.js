import React from 'react';
import {render} from 'react-dom';

import reducer from './reducers';
import {Provider} from 'react-redux';
import {createStore} from 'redux';

import App from './components/App.js';
import About from './components/about/About';
import Home from './components/home/Home';
import store from './stores/dashboardStore';

import {
  Router,
  Route,
  Link,
  hashHistory,
  IndexRoute,
  Redirect
} from 'react-router';

// Render the main component into the dom
render((
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <Route path="/about" component={About}/>
        <Route path="/home" component={Home}/>
        <IndexRoute component={Home}/>
      </Route>
    </Router>
  </Provider>
), document.getElementById('app'));
