import axios from 'axios';

export function fetchDasboard() {
    return function (dispatch) {
        axios
            .get('./mock/dashboard.json')
            .then((response) => {
                dispatch({type: 'FINANCE_LOADED', payload: response.data});
                dispatch({type: 'USER_LOADED', payload: response.data});
            })
            .catch((err) => {
                dispatch({type: 'FINANCE_REJECTED', payload: err});
                dispatch({type: 'USER_REJECTED', payload: err});
            });
    }
}