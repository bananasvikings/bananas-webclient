import {combineReducers } from "redux";
import finances from "./finances";
import user from "./user";
 
const financeApp = combineReducers({
  finances,
  user
});

export default financeApp;