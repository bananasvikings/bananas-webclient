const financeInitialState = {
    'from': '01-01-2016',
    'to': '01-05-2016',

    'expensesToday': [
      
    ]
  };
  
const finances = (state = financeInitialState, action) => {
  switch (action.type) {
    case 'FINANCE_LOADED':
      return  action.payload
    case 'FINANCE_REJECTED':
      return {state}
    default:
      return state
  }
}

export default finances;