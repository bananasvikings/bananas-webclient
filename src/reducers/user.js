const userInitialState = {name: "User", address: "Address"};

const user = (state = userInitialState, action) => {
    switch (action.type) {
        case 'USER_LOADED':
            return state
        case 'USER_REJECTED':
            return state
        default:
            return state
    }
}

export default user;