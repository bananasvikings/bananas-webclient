import React, {Component} from 'react';
import {connect} from 'react-redux';
// Actions
import * as dashboardActions from '../../actions/dashboardActions'

// Compoents
import TodayExpenseOverview from '../portlets/expenses/TodayExpenseOverview';
import Cards from '../cards/CardsDashboard';

@connect((store) => {
    return {user: store.user, finances: store.finances}
})
class Home extends Component {
    componentWillMount() {
        this
            .props
            .dispatch(dashboardActions.fetchDasboard());
    }

    render() {
        const {finances} = this.props;

        return (
            <div>
                <Cards></Cards>
                <div className='row'>
                    <div className='col-lg-6 col-xs-12 col-sm-12'>
                        <TodayExpenseOverview expensesToday={finances.expensesToday}></TodayExpenseOverview>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;