import React, {PropTypes} from 'react';

const TodayExpenseItem = ({description, value} ) => {
    return (
        <li>
            <div className='col1'>
                <div className='cont'>
                    <div className='cont-col1'>
                        <div className='label label-sm label-info'>
                            <i className='fa fa-check'></i>
                        </div>
                    </div>
                    <div className='cont-col2'>
                        <div className='desc'>
                            
                            {description} &nbsp;

                            <span className='label label-sm label-warning '>
                                Take action
                                <i className='fa fa-share'></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div className='col2'>
                <div className='date'>
                   {value}
                </div>
            </div>
        </li>
    )
}

TodayExpenseItem.propTypes = {
  description: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
}

export default TodayExpenseItem