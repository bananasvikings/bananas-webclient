import React from 'react';

import TodayExpenseItem from './TodayExpenseItem';

import './TodayExpenseOverview.css';

const TodayExpenseOverview = ({expensesToday}) => {
    return (
        <div className='portlet light tasks-widget bordered ChartExpenseOverview'>
            <div className='portlet-title'>
                <div className='caption'>
                    <i className='icon-share font-dark hide'></i>
                    <span className='caption-subject font-dark bold uppercase'>Expenses today
                    </span>
                    <span className='caption-helper'>&nbsp;expenses of 26/04/2016.</span>
                </div>
            </div>
            <div className='portlet-body'>
                <div className='task-content'>
                    <div className='slimScrollDiv'>
                        <div
                            className='scroller'
                            data-always-visible='1'
                            data-rail-visible1='1'
                            data-initialized='1'>
                            <ul className='feeds'>
                                {expensesToday.map(expense => <TodayExpenseItem description={expense.description} value={expense.value}></TodayExpenseItem>)}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TodayExpenseOverview;