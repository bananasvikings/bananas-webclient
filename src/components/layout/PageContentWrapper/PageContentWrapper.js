import React, {Component} from 'react';

class PageContentWrapper extends Component {
    render() {
        return (
            <div className='page-content-wrapper'>
                <div className='page-content'>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default PageContentWrapper;