import React, {Component} from 'react';
import TopMenu from '../TopMenu/TopMenu';

class PageHeader extends Component {
    render() {
        return (
            <div className='page-header navbar navbar-fixed-top'>
                <div className='page-header-inner '>
                    <div className='page-logo'>
                        <a href='index.html'>
                            <img src='../assets/layouts/layout/img/logo.png' alt='logo' className='logo-default'/>
                        </a>
                        <div className='menu-toggler sidebar-toggler'>
                            <span></span>
                        </div>
                    </div>
                    <a href='javascript:;' className='menu-toggler responsive-toggler' data-toggle='collapse' data-target='.navbar-collapse'>
                        <span></span>
                    </a>
                    <TopMenu>
                    </TopMenu>
                </div>
            </div>
        );
    }
}

export default PageHeader;