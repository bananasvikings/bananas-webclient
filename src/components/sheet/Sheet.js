import React from 'react';
import moment from 'moment';

export default class Sheet extends React.Component {
  constructor(props) {
    super(props);
  }

  generatePeriodRange(from, to) {
    var from = moment(from),
      cellsCount = Math.abs(from.diff(to, 'months')),
      months = [];

    this.cellsCount = cellsCount;

    for (var c = 0; c < this.cellsCount; c++) {
      months.push(from.add(1, 'months').format('MMMM/YYYY'));
    }
    return months;
  }

  generateMonthsFromTo(categories, from, to) {

    var tableContent = [],
      incomes = this.generateIncomes(categories, from, to),

      expenses = this.generateExpenses(categories, from, to);

    tableContent.push(incomes);

    tableContent.push(expenses);

    return tableContent;

  }

  generateInputFields(rowLabel, from, to) {
    var cellsCount = Math.abs(moment(from).diff(to, 'months')),
      rows = [];

    for (var c = 0; c < cellsCount; c++) {
      rows.push((<td><input></input></td>));
    }
    return rows;
  }

  generateIncomes(categories, from, to) {

    var returnValues = [];


    returnValues.push((<tr>
      <td className='info' colSpan={this.cellsCount + 1}>Incomes</td>
    </tr>));

    returnValues.push(categories
      .incomes
      .map((income, index) => {
        const inputFields = this.generateInputFields(income, from, to);

        return (
          <tr key={index}>
            <td>{income}</td>
            {inputFields}
          </tr>
        )
      }));

    return returnValues;
  }

  generateExpenses(categories, from, to) {
    var returnValues = [];


    returnValues.push((<tr>
      <td className='info' colSpan={this.cellsCount + 1}>Expenses</td>
    </tr>));

    returnValues.push(categories
      .expenses
      .map((expense, index) => {
        const inputFields = this.generateInputFields(expense, from, to);

        return (
          <tr key={index}>
            <td>{expense}</td>
            {inputFields}
          </tr>
        )
      }));

    return returnValues;
  }


  getObectCategory() {
    return {
      incomes: ['Salary','Freelancers'],
      expenses: ['House loan', 'Fun', 'Dining out', 'Traveling', 'Doing stuff']
    };
  }

  render() {
    this.months = this.generatePeriodRange(this.props.from, this.props.to);

    const monthsRender = this
      .months
      .map((month, index) => (
        <td key={index}>{month}</td>
      ));

    let categories = this.getObectCategory();

    const categoriesRender = this.generateMonthsFromTo(categories, this.props.from, this.props.to);

    return (
      <table key='sheet-finance' className='table table-striped table-bordered'>
        <thead>
        <tr>
          <td>&nbsp;</td>
          {monthsRender}</tr>
        </thead>
        <tbody>
        {categoriesRender}
        </tbody>
      </table>
    );
  }
}
