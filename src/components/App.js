import React from 'react';
import Sheet from './sheet/Sheet';
import PageHeader from './layout/PageHeader/PageHeader';
import PageContentWrapper from './layout/PageContentWrapper/PageContentWrapper';
import PageSideBarMenu from './layout/PageSideBar/PageSideBarMenu';

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var sizePageContent =  {
      minHeight: 1112
    };

    return (
      <div style={sizePageContent}>
        <PageHeader></PageHeader>

        <div className='page-container' style={sizePageContent}>
          <PageSideBarMenu></PageSideBarMenu>
          <PageContentWrapper>
            {this.props.children}
          </PageContentWrapper>
        </div>
      </div>
    );
  }
}
